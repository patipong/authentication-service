const express = require('express')
const router = express.Router()

router.post('/', require('./userLogin'))

module.exports = router