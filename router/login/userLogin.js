const Model = require('../../model/auth')
const JWT = require('jsonwebtoken')

const TAG = 'userLogin'

module.exports = async (req, res) => {
  let { responsed } = req
  responsed.method = TAG

  let { body: { email, password } } = req

  if (!Boolean(email && password)) {
    return res.send(Object.assign({}, responsed, {
      error: { code: `${TAG}-request-body`, message: `Metood ${TAG} request body` }
    }))
  }

  try {
    let data = await Model.getUserInfo({ email, password })
    let user = data.rows && data.rows[0]

    let input = { customer_id: user.customer_id, email: user.email, name: user.name, create_date: user.create_date, clientid: user.clientid }
    let token = JWT.sign(input, user.secretkey, { expiresIn: '1y' })

    return res.send(Object.assign({}, responsed, {
      result: true,
      data: { token, user }
    }))

  } catch (error) {
    console.error(`TAG ${TAG}`, error)
    return res.send(Object.assign({}, responsed, {
      error: { code: `${TAG}-permission-denied`, message: `Metood ${TAG} permission denied` }
    }))

  }
}