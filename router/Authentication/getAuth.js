const _ = require('lodash')
const JWT = require('jsonwebtoken')

const Model = require('../../model/auth')

const TAG = 'getAuth'

module.exports = async (req, res, next) => {
  let { responsed } = req
  responsed.method = TAG

  try {
    let _token = req.header('token')
    let _decodeToken = JWT.decode(_token)

    if (!Boolean(_token) || _.isEmpty(_decodeToken)) {
      return res.send(Object.assign({}, responsed, {
        error: { code: `${TAG}-permission-denied`, message: `Metood ${TAG} permission denied` }
      }))
    }

    let data = await Model.verifyUserToken({ customer_id: _decodeToken.customer_id, clientid: _decodeToken.clientid })
    data = data.rows && data.rows[0]

    let _tokenVerify = JWT.verify(_token, data.secretkey, (err, decode) => (decode || err))
    if (_tokenVerify.message)
      return res.send(Object.assign({}, responsed, {
        error: { code: `${TAG}-token-invalid`, message: `Metood ${TAG} token invalid` }
      }))

    return res.send(Object.assign({}, responsed, {
      result: true,
      data: _.pick(data, ['customer_id', 'name', 'last_name', 'email', 'phone', 'create_date', 'clientid'])
    }))

  } catch (error) {
    console.error(`TAG ${TAG}`, error)
    return res.send(Object.assign({}, responsed, {
      error: { code: `${TAG}-permission-denied`, message: `Metood ${TAG} permission denied` }
    }))

  }
}