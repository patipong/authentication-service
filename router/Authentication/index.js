const express = require('express')
const router = express.Router()

router.get('/', require('./getAuth'))
router.post('/', require('./createAuth'))

module.exports = router