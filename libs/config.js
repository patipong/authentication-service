const getConfig = () => {
  const { DATABASE_HOST = 'localhost', DATABASE_PORT = 5432, DATABASE_USER = 'postgres', DATABASE_NAME = 'Service_A', salt = '.7p@ssword' } = process.env
  return {
    database: {
      host: DATABASE_HOST,
      port: DATABASE_PORT,
      user: DATABASE_USER,
      database: DATABASE_NAME,
      salt: salt
    }
  }
}

module.exports = getConfig()