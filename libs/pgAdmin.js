const { Pool } = require('pg')
const sha1 = require('sha1')

const Config = require('./config')

var PGAdmin = function () {
  this.sql = ''
  this.pool = new Pool(Config.database)
}

PGAdmin.prototype.STATEMENT = function (statement) {
  statement.name = `${statement.name}_${sha1(statement.text + new Date().getTime()).substring(0, 8)}`
  this.sql = statement
  return this
}

PGAdmin.prototype.query = function (sql) {
  return new Promise((in_resolve, in_reject) => {
    this.pool.connect((err, client, release) => {
      if (err) {
        client && client.release()
        return in_reject(err);
      }

      let result = client.query(sql || this.sql)

      result.then(() => client.release()).catch(() => client.release())
      result.then(in_resolve).catch(in_reject)
    })
  })
}

module.exports = PGAdmin