function DEBUG() {
  let data = [`DEBUG > ${new Date().getTime()} >`, ...arguments]
  console.log('\x1b[32m%s\x1b[0m', data.join(' '))
}

module.exports = {
  DEBUG: DEBUG
}