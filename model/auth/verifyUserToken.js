const PGAdmin = require('../../libs/pgAdmin')

module.exports = ({ customer_id, clientid }) => {
  const pg = new PGAdmin()
  return pg.STATEMENT({
    name: 'verifyUserToken',
    text: `SELECT * FROM public.customer_info WHERE customer_id = $1 AND clientid = $2`,
    values: [customer_id, clientid]
  }).query()
}