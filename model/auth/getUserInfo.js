const PGAdmin = require('../../libs/pgAdmin')

module.exports = ({ email, password }) => {
  let values = []
  let where = ''

  if (email)
    where += `AND email = $${values.push(email)} `

  if (password)
    where += `AND password = $${values.push(password)} `

  const pg = new PGAdmin()
  return pg.STATEMENT({
    name: 'getUserInfo',
    text: ` SELECT *
            FROM public.customer_info
            WHERE TRUE
            ${where}`,
    values: values
  }).query()
}