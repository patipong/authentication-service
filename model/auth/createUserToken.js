const PGAdmin = require('../../libs/pgAdmin')

module.exports = ({ email, password, name, last_name, phone }) => {
  const pg = new PGAdmin()
  return pg.STATEMENT({
    name: 'createUserToken',
    text: `
    INSERT INTO public.customer_info(email, password, name, last_name, phone)
         VALUES ($1, $2, $3, $4, $5)
      RETURNING customer_id, email, create_date, clientid, secretkey
    `,
    values: [email, password, name, last_name, phone]
  }).query()
}