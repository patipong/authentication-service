const OS = require('os')
const DNS = require('dns')

const express = require('express')
const bodyParser = require('body-parser')
const router_express = express()

const Log = require('./libs/log')
const PGAdmin = require('./libs/pgAdmin')

require('dotenv').config();

var NODE = () => {
  let port = process.env.PORT || 6000
  router_express.listen(port)

  DNS.lookup(OS.hostname(), (err, add, fam) => {
    Log.DEBUG(`SERVICE START`)
    Log.DEBUG(`Address : ${add}:${port}`)
  })

  router_express.use(bodyParser.json({ limit: '5mb' }))
  router_express.use(bodyParser.urlencoded({ extended: true }))

  router_express.use((req, res, next) => {
    req.responsed = { method: '', result: false, eventDate: new Date(), route: `${req && req.method} ${req && req.path}`, }
    if (req.method !== 'GET')
      Log.DEBUG(JSON.stringify(req.body))

    return next()
  })

  router_express.options('*', (req, res) => res.sendStatus(200))
  router_express.use('/authentication', require('./router/Authentication'))
  router_express.use('/login', require('./router/login'))
  router_express.all('*', (req, res) => res.sendStatus(403))
}

NODE();